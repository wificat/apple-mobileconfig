# Apple .mobileconfig



Plist XML

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>PayloadContent</key>
	<array>
		<dict>
			<key>AutoJoin</key>
			<true/>
			<key>CaptiveBypass</key>
			<false/>
			<key>EAPClientConfiguration</key>
			<dict>
				<key>AcceptEAPTypes</key>
				<array>
					<integer>25</integer>
				</array>
				<key>OuterIdentity</key>
				<string>anonymous</string>
				<key>PayloadCertificateAnchorUUID</key>
				<array>
					<string>91F58605-634D-4363-ACEE-900456C954E3</string>
				</array>
				<key>TLSTrustedServerNames</key>
				<array>
					<string><!-- DNS SUFFIX HERE --></string>
				</array>
			</dict>
			<key>EncryptionType</key>
			<string>WPA2</string>
			<key>HIDDEN_NETWORK</key>
			<false/>
			<key>IsHotspot</key>
			<false/>
			<key>PayloadDescription</key>
			<string>WLAN-Einstellungen konfigurieren</string>
			<key>PayloadDisplayName</key>
			<string>WLAN</string>
			<key>PayloadIdentifier</key>
          	<!-- Payload UUID #1 -->
			<string>com.apple.wifi.managed.0815204B-6E84-4B64-BD12-837594FA1850</string>
			<key>PayloadType</key>
			<string>com.apple.wifi.managed</string>
			<key>PayloadUUID</key>
          	<!-- Payload UUID #1 (same as above!) -->
			<string>0815204B-6E84-4B64-BD12-837594FA1850</string>
			<key>PayloadVersion</key>
			<integer>1</integer>
			<key>ProxyType</key>
			<string>None</string>
			<key>SSID_STR</key>
			<string><!-- SSID HERE --></string>
		</dict>
		<dict>
			<key>AutoJoin</key>
			<false/>
			<key>CaptiveBypass</key>
			<false/>
			<key>EncryptionType</key>
			<string>None</string>
			<key>HIDDEN_NETWORK</key>
			<false/>
			<key>IsHotspot</key>
			<false/>
			<key>PayloadDescription</key>
			<string>WLAN-Einstellungen konfigurieren</string>
			<key>PayloadDisplayName</key>
			<string>WLAN</string>
			<key>PayloadIdentifier</key>
          	<!-- Payload UUID #2 -->
			<string>com.apple.wifi.managed.138DCD96-48B1-4BEA-A1E5-0775B6A987C7</string>
			<key>PayloadType</key>
			<string>com.apple.wifi.managed</string>
			<key>PayloadUUID</key>
          	<!-- Payload UUID #2 (same as above!) -->
			<string>138DCD96-48B1-4BEA-A1E5-0775B6A987C7</string>
			<key>PayloadVersion</key>
			<integer>1</integer>
			<key>ProxyType</key>
			<string>None</string>
			<key>SSID_STR</key>
			<string><!-- SSID TO DELETE --></string>
		</dict>
		<dict>
			<key>PayloadCertificateFileName</key>
			<string>ca.cer</string>
			<key>PayloadContent</key>
			<data>
			<!-- CERTIFICATE BASE64 -->
			</data>
			<key>PayloadDescription</key>
			<string>Add Root CA-Certificate</string>
			<key>PayloadDisplayName</key>
			<string><!-- CERTIFICATE CN NAME --></string>
			<key>PayloadIdentifier</key>
          	<!-- Payload UUID #3 -->
			<string>com.apple.security.root.91F58605-634D-4363-ACEE-900456C954E3</string>
			<key>PayloadType</key>
			<string>com.apple.security.root</string>
			<key>PayloadUUID</key>
          	<!-- Payload UUID #3 (same as above!) -->
			<string>91F58605-634D-4363-ACEE-900456C954E3</string>
			<key>PayloadVersion</key>
			<integer>1</integer>
		</dict>
	</array>
	<key>PayloadDescription</key>
	<string>Wireless Network Profile for SSID Network</string>
	<key>PayloadDisplayName</key>
	<string>SSID Wifi Profile</string>
	<key>PayloadIdentifier</key>
  	<!-- New UUID  -->
	<string>SSID.57619DDF-D197-4E25-9D0E-E8A5CB0F0546</string>
	<key>PayloadOrganization</key>
	<string><!-- ORGANIZATION NAME --></string>
	<key>PayloadRemovalDisallowed</key>
	<false/>
	<key>PayloadType</key>
	<string>Configuration</string>
	<key>PayloadUUID</key>
  	<!-- New UUID -->
	<string>6AE8AC7B-8D93-4D33-A1D5-5115E13460B3</string>
	<key>PayloadVersion</key>
	<integer>1</integer>
</dict>
</plist>
```

## Sign XML

```sh
openssl smime -sign -signer certificate.crt -inkey certificate.key -certfile 1_Intermediate-CA.crt -nodetach -outform der -in sample-ssid_wifi_profile.mobileconfig -out sample-ssid_wifi_profile_signed.mobileconfig
```



